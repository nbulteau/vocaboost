README for vocaboost
==========================
Keywords : Java 8, Spring Boot, Lombok, Maven, Tomcat, AngularJS, Yeoman, Bower, Grunt, WebSocket (atmosphere)

Installer java 8 : http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Installer maven : http://maven.apache.org/download.cgi

Installer mongodb : http://www.mongodb.org/downloads

Lancer mongodb 
Par exemple :
-> cd [mongodb install]\bin
-> mongod --dbpath "D:\MongoDB Datafiles"

Lancement de l'application
====
-> cd [vocaboost]

Generation du livrable : 
-> mvn -Pprod package

Lancement de vocaboost :
En mode developpement :
-> mvn spring-boot:run
-> Dans un navigateur : http://localhost:8080/

En mode production :
-> mvn -Pprod spring-boot:run

Lancement sans serveur d'application :
-> java -jar vocaboost-0.1.0.war
ou
-> java -jar vocaboost-0.1.0.war --spring.profiles.active=prod

Pour utiliser un fichier de config externalisé : --spring.config.location
exemple : -Dspring.config.location=file:/D:/tmp/application.yml, classpath:/config/application.yml

le vocaboost-backoffice-0.1.0.war.original peut etre deployé directement dans un Tomcat

Profiles
====

- Dev : SPRING_PROFILE_DEVELOPMENT (Profile par default)
=> Logging par AspectJ 

- Prod : SPRING_PROFILE_PRODUCTION
=> Mise en place d'un cache HTTP pour les resources statiques


