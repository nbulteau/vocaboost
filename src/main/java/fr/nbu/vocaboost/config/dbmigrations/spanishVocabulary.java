package fr.nbu.vocaboost.config.dbmigrations;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates old vocaboost lists
 */
@ChangeLog(order = "003")
public class spanishVocabulary {

    @ChangeSet(order = "01", author = "admin", id = "Spanish-Familly")
    public void addFamillyVocalulary(DB db) {
        DBCollection vocaBoostListCollection = db.getCollection("vocaBoostList");

        List<BasicDBObject> items = new ArrayList<>();
        items.add(new BasicDBObject().append("toLearn", "Los vínculos / lazos familiares ").append("answer", "les liens familliaux"));
        items.add(new BasicDBObject().append("toLearn", "Los padres ").append("answer", "les parents"));
        items.add(new BasicDBObject().append("toLearn", "Los abuelos").append("answer", "les grand-parents"));
        items.add(new BasicDBObject().append("toLearn", "El padre").append("answer", "le père "));
        items.add(new BasicDBObject().append("toLearn", "La madre").append("answer", "la mère"));
        items.add(new BasicDBObject().append("toLearn", "El/la abuelo,a").append("answer", "le grand-père, la grand-mère"));
        items.add(new BasicDBObject().append("toLearn", "El /la hijo,a ").append("answer", "le fils, la fille"));
        items.add(new BasicDBObject().append("toLearn", "El/la bisabuelo,a ").append("answer", "l’arrière grand-père, grand-mère"));
        items.add(new BasicDBObject().append("toLearn", "El/la nieto,a").append("answer", "le petit fils, la petite fille"));
        items.add(new BasicDBObject().append("toLearn", "El/la bisnieto,a ").append("answer", "l’arrière petit-fils, petite-fille"));
        items.add(new BasicDBObject().append("toLearn", "El/la tatarabuelo,a").append("answer", "l’arrière arrière grand-père, grand-mère"));
        items.add(new BasicDBObject().append("toLearn", "El/la tataranieto,a").append("answer", "l’arrière arrière petit-fils, petite-fille"));
        items.add(new BasicDBObject().append("toLearn", "El tío ").append("answer", "l’oncle/"));
        items.add(new BasicDBObject().append("toLearn", "La tía").append("answer", "la tante"));
        items.add(new BasicDBObject().append("toLearn", "El/la sobrino,a").append("answer", "le neveu, la nièce"));
        items.add(new BasicDBObject().append("toLearn", "El padrino").append("answer", "le parrain"));
        items.add(new BasicDBObject().append("toLearn", "La madrina").append("answer", "la marraine"));
        items.add(new BasicDBObject().append("toLearn", "El/la ahijado,a").append("answer", "le filleul, la filleule"));
        items.add(new BasicDBObject().append("toLearn", "El/la primo,a ").append("answer", "le cousin,la cousine"));
        items.add(new BasicDBObject().append("toLearn", "El/la primo,a hermano,a").append("answer", "le cousin germain,la cousine germaine"));
        items.add(new BasicDBObject().append("toLearn", "El/la primo,a segundo,a ").append("answer", "l’arrière cousin,cousine"));
        items.add(new BasicDBObject().append("toLearn", "El/la cuñado,a").append("answer", "le beau frère,la belle soeur"));
        items.add(new BasicDBObject().append("toLearn", "El yerno").append("answer", "le beau fils"));
        items.add(new BasicDBObject().append("toLearn", "La nuera").append("answer", "la belle fille"));
        items.add(new BasicDBObject().append("toLearn", "El/la hermano,a").append("answer", "le frère, la soeur"));
        items.add(new BasicDBObject().append("toLearn", "Una familia recompuesta").append("answer", "une famille recomposée"));
        items.add(new BasicDBObject().append("toLearn", "El padrastro").append("answer", "le beau père"));
        items.add(new BasicDBObject().append("toLearn", "La madrastra").append("answer", "la belle-mère"));
        items.add(new BasicDBObject().append("toLearn", "El esposo").append("answer", "le mari"));
        items.add(new BasicDBObject().append("toLearn", "La esposa").append("answer", "la femme"));
        items.add(new BasicDBObject().append("toLearn", "El matrimonio").append("answer", "le mariage"));
        items.add(new BasicDBObject().append("toLearn", "La convivencia").append("answer", "le concubinage"));
        items.add(new BasicDBObject().append("toLearn", "Soltero,a ").append("answer", "célibataire"));
        items.add(new BasicDBObject().append("toLearn", "Casado,a").append("answer", "marié,e"));
        items.add(new BasicDBObject().append("toLearn", "Casarse ").append("answer", "semarier"));
        items.add(new BasicDBObject().append("toLearn", "Divorciarse").append("answer", "divorcer"));
        items.add(new BasicDBObject().append("toLearn", "Adoptar").append("answer", "adopter"));
        items.add(new BasicDBObject().append("toLearn", "Quedarse en estado").append("answer", "tomber enceinte"));
        items.add(new BasicDBObject().append("toLearn", "Dar a luz").append("answer", "accoucher"));
        items.add(new BasicDBObject().append("toLearn", "Bautizar").append("answer", "baptiser"));
        items.add(new BasicDBObject().append("toLearn", "Hacer vida marital").append("answer", "vivre en concubinage"));
        items.add(new BasicDBObject().append("toLearn", "Comprometerse").append("answer", "se fiancer"));


        vocaBoostListCollection.insert(
            BasicDBObjectBuilder.start()
                .add("name", "Vocabulaire Espagnol")
                .add("description", "Vocabulaire sélectif à connaître : La familia /La famille")
                .add("language", "es")
                .add("sharingMode", "PUBLIC")
                .add("vocaBoostItems", items)
                .add("createdBy", "admin")
                .add("lastModifiedBy", "admin")
                .get());
    }

    @ChangeSet(order = "02", author = "admin", id = "sSpanish-House")
    public void addHouseVocalulary(DB db) {
        DBCollection vocaBoostListCollection = db.getCollection("vocaBoostList");

        List<BasicDBObject> items = new ArrayList<>();
        items.add(new BasicDBObject().append("toLearn", "Una casa").append("answer", "une maison"));
        items.add(new BasicDBObject().append("toLearn", "Un chalé adosado").append("answer", "une maison adossée"));
        items.add(new BasicDBObject().append("toLearn", "Un piso").append("answer", "un apartement"));
        items.add(new BasicDBObject().append("toLearn", "Un apartamento/ un estudio").append("answer", "un studio"));
        items.add(new BasicDBObject().append("toLearn", "Un casa de dos plantas").append("answer", "une maison à deux étages"));
        items.add(new BasicDBObject().append("toLearn", "La planta baja").append("answer", "le rez-de-chaussée"));
        items.add(new BasicDBObject().append("toLearn", "La primera planta").append("answer", "le premier étage"));
        items.add(new BasicDBObject().append("toLearn", "Un edificio").append("answer", "un immeuble"));
        items.add(new BasicDBObject().append("toLearn", "Un barrio").append("answer", "un quartier"));
        items.add(new BasicDBObject().append("toLearn", "La escalera").append("answer", "les escaliers"));
        items.add(new BasicDBObject().append("toLearn", "El ascensor").append("answer", "l’ascenceur"));
        items.add(new BasicDBObject().append("toLearn", "El balcón").append("answer", "le balcon"));
        items.add(new BasicDBObject().append("toLearn", "La terraza").append("answer", "la terrasse (fermée)"));
        items.add(new BasicDBObject().append("toLearn", "La buhardilla").append("answer", "le grenier"));
        items.add(new BasicDBObject().append("toLearn", "El sótano").append("answer", "la cave"));
        items.add(new BasicDBObject().append("toLearn", "La cocina").append("answer", "la cuisine"));
        items.add(new BasicDBObject().append("toLearn", "El comedor").append("answer", "la salle à manger"));
        items.add(new BasicDBObject().append("toLearn", "El salón").append("answer", "le salon"));
        items.add(new BasicDBObject().append("toLearn", "El dormitorio/la habitación").append("answer", "la chambre"));
        items.add(new BasicDBObject().append("toLearn", "El cuarto de baño").append("answer", "la salle de bains"));
        items.add(new BasicDBObject().append("toLearn", "El retrete").append("answer", "les toilettes"));
        items.add(new BasicDBObject().append("toLearn", "El pasillo").append("answer", "le couloir"));
        items.add(new BasicDBObject().append("toLearn", "El recibidor").append("answer", "le vestibule"));
        items.add(new BasicDBObject().append("toLearn", "Los muebles").append("answer", "les meubles"));
        items.add(new BasicDBObject().append("toLearn", "El sofá").append("answer", "le canapé"));
        items.add(new BasicDBObject().append("toLearn", "El sillón/ la butaca").append("answer", "le fauteuil"));
        items.add(new BasicDBObject().append("toLearn", "La mesa").append("answer", "la table"));
        items.add(new BasicDBObject().append("toLearn", "Las sillas").append("answer", "les chaises"));
        items.add(new BasicDBObject().append("toLearn", "Una lámpara").append("answer", "une lampe"));
        items.add(new BasicDBObject().append("toLearn", "Una alfombra").append("answer", "un tapis"));
        items.add(new BasicDBObject().append("toLearn", "Una estantería").append("answer", "une étagère"));
        items.add(new BasicDBObject().append("toLearn", "Las cortinas").append("answer", "les rideaux"));
        items.add(new BasicDBObject().append("toLearn", "La lavadora").append("answer", "la machine à laver"));
        items.add(new BasicDBObject().append("toLearn", "El lavavajilla").append("answer", "le lave -vaisselle"));
        items.add(new BasicDBObject().append("toLearn", "El  horno").append("answer", "le four"));
        items.add(new BasicDBObject().append("toLearn", "La nevera").append("answer", "le réfrigérateur"));
        items.add(new BasicDBObject().append("toLearn", "El lavabo").append("answer", "l’évier"));
        items.add(new BasicDBObject().append("toLearn", "El inodoro").append("answer", "le WC"));
        items.add(new BasicDBObject().append("toLearn", "El espejo").append("answer", "le mirroir"));
        items.add(new BasicDBObject().append("toLearn", "La ventana").append("answer", "la fenêtre"));
        items.add(new BasicDBObject().append("toLearn", "La  cama ").append("answer", "le lit"));
        items.add(new BasicDBObject().append("toLearn", "La litera").append("answer", "les lits jumeaux"));
        items.add(new BasicDBObject().append("toLearn", "Un baúl ").append("answer", "un coffre"));
        items.add(new BasicDBObject().append("toLearn", "La calefacción").append("answer", "le chauffage"));
        items.add(new BasicDBObject().append("toLearn", "El aire acondicionado").append("answer", "l’air conditionné"));
        items.add(new BasicDBObject().append("toLearn", "La bañera").append("answer", "la baignoire"));
        items.add(new BasicDBObject().append("toLearn", "Mudarse ").append("answer", "déménager"));
        items.add(new BasicDBObject().append("toLearn", "Alquilar").append("answer", "louer"));
        items.add(new BasicDBObject().append("toLearn", "Comprar ").append("answer", "acheter"));
        items.add(new BasicDBObject().append("toLearn", "Reformar").append("answer", "faire des travaux"));
        items.add(new BasicDBObject().append("toLearn", "Pintar").append("answer", "peindre"));
        items.add(new BasicDBObject().append("toLearn", "Compartir piso ").append("answer", "partager un appartement"));
        items.add(new BasicDBObject().append("toLearn", "Depositar una fianza").append("answer", "déposer une caution"));
        items.add(new BasicDBObject().append("toLearn", "Avalar").append("answer", "se porter caution"));

        vocaBoostListCollection.insert(
            BasicDBObjectBuilder.start()
                .add("name", "Vocabulaire Espagnol")
                .add("description", "Vocabulaire sélectif à connaître : La casa/ la maison")
                .add("language", "es")
                .add("sharingMode", "PUBLIC")
                .add("vocaBoostItems", items)
                .add("createdBy", "admin")
                .add("lastModifiedBy", "admin")
                .get());
    }

}
