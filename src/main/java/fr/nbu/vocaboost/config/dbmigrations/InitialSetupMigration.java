package fr.nbu.vocaboost.config.dbmigrations;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Creates the initial database setup
 */
@ChangeLog(order = "001")
public class InitialSetupMigration {

    private Map<String, String>[] authoritiesUser = new Map[]{new HashMap<>()};

    private Map<String, String>[] authoritiesAdminAndUser = new Map[]{new HashMap<>(), new HashMap<>()};

    {
        authoritiesUser[0].put("_id", "ROLE_USER");
        authoritiesAdminAndUser[0].put("_id", "ROLE_USER");
        authoritiesAdminAndUser[1].put("_id", "ROLE_ADMIN");
    }

    @ChangeSet(order = "01", author = "initiator", id = "01-addAuthorities")
    public void addAuthorities(DB db) {
        DBCollection authorityCollection = db.getCollection("authority");
        authorityCollection.insert(
            BasicDBObjectBuilder.start()
                .add("_id", "ROLE_ADMIN")
                .get());
        authorityCollection.insert(
            BasicDBObjectBuilder.start()
                .add("_id", "ROLE_USER")
                .get());
    }

    @ChangeSet(order = "02", author = "initiator", id = "02-addUsers")
    public void addUsers(DB db) {
        DBCollection usersCollection = db.getCollection("user");
        usersCollection.createIndex("login");
        usersCollection.createIndex("email");

        usersCollection.insert(BasicDBObjectBuilder.start()
            .add("_id", "system")
            .add("password", "$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG")
            .add("firstName", "")
            .add("lastName", "System")
            .add("email", "")
            .add("activated", "true")
            .add("langKey", "fr")
            .add("createdBy", "system")
            .add("createdDate", new Date())
            .add("authorities", authoritiesAdminAndUser)
            .get());

        usersCollection.insert(BasicDBObjectBuilder.start()
            .add("_id", "admin")
            .add("password", "$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC")
            .add("firstName", "")
            .add("lastName", "Administrator")
            .add("email", "")
            .add("activated", "true")
            .add("langKey", "fr")
            .add("createdBy", "system")
            .add("createdDate", new Date())
            .add("authorities", authoritiesAdminAndUser)
            .get());

        usersCollection.insert(BasicDBObjectBuilder.start()
            .add("_id", "teo.bulteau")
            .add("password", "$2a$10$GfciQUhwsU2cD3qTLFGSv.2OxxXpntYVDqrNh1ejk1Fi0DVjVyNie")
            .add("firstName", "Teo")
            .add("lastName", "Bulteau")
            .add("email", "teo.bulteau@gmail.com")
            .add("activated", "true")
            .add("langKey", "fr")
            .add("createdBy", "system")
            .add("createdDate", new Date())
            .add("authorities", authoritiesUser)
            .get());

        usersCollection.insert(BasicDBObjectBuilder.start()
            .add("_id", "eve.bulteau")
            .add("password", "$2a$10$GfciQUhwsU2cD3qTLFGSv.2OxxXpntYVDqrNh1ejk1Fi0DVjVyNie")
            .add("firstName", "Eve")
            .add("lastName", "Bulteau")
            .add("email", "eve.bulteau@gmail.com")
            .add("activated", "true")
            .add("langKey", "fr")
            .add("createdBy", "system")
            .add("createdDate", new Date())
            .add("authorities", authoritiesUser)
            .get());

    }
    @ChangeSet(author = "initiator", id = "03-addSocialUserConnection", order = "03")
    public void addSocialUserConnection(DB db) {
        DBCollection socialUserConnectionCollection = db.getCollection("social_user_connection");
        socialUserConnectionCollection.createIndex(BasicDBObjectBuilder
                .start("user_id", 1)
                .add("provider_id", 1)
                .add("provider_user_id", 1)
                .get(),
            "user-prov-provusr-idx", true);
    }
}
