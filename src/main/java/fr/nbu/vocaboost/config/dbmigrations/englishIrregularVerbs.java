package fr.nbu.vocaboost.config.dbmigrations;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates old vocaboost lists
 */
@ChangeLog(order = "003")
public class englishIrregularVerbs {

    @ChangeSet(order = "01", author = "admin", id = "English-IrregularVerbsLevel3")
    public void addEnglishIrregularVerbsListLevel3(DB db) {
        DBCollection vocaBoostListCollection = db.getCollection("vocaBoostList");

        List<BasicDBObject> items = new ArrayList<>();
        items.add(new BasicDBObject().append("toLearn", "arise").append("answer", "arose - arisen - s'élever, survenir"));
        items.add(new BasicDBObject().append("toLearn", "awake").append("answer", "awoke - awoken - (se) réveiller"));
        items.add(new BasicDBObject().append("toLearn", "bear").append("answer", "bore - borne - supporter"));
        items.add(new BasicDBObject().append("toLearn", "beat").append("answer", "beat - beaten - battre"));
        items.add(new BasicDBObject().append("toLearn", "become").append("answer", "became - become - devenir"));
        items.add(new BasicDBObject().append("toLearn", "begin").append("answer", "began - begun - commencer"));
        items.add(new BasicDBObject().append("toLearn", "bend").append("answer", "bent - bent - (se) courber"));
        items.add(new BasicDBObject().append("toLearn", "bet").append("answer", "bet - bet - parier"));
        items.add(new BasicDBObject().append("toLearn", "bind").append("answer", "bound - bound - lier, relier"));
        items.add(new BasicDBObject().append("toLearn", "bite").append("answer", "bit - bitten - mordre"));
        items.add(new BasicDBObject().append("toLearn", "bleed").append("answer", "bled - bled - saigner"));
        items.add(new BasicDBObject().append("toLearn", "blow").append("answer", "blew - blown - souffler"));
        items.add(new BasicDBObject().append("toLearn", "break").append("answer", "broke - broken - casser"));
        items.add(new BasicDBObject().append("toLearn", "breed").append("answer", "bred - bred - élever (du bétail)"));
        items.add(new BasicDBObject().append("toLearn", "bring").append("answer", "brought - brought - apporter"));
        items.add(new BasicDBObject().append("toLearn", "build").append("answer", "built - built - construire"));
        items.add(new BasicDBObject().append("toLearn", "burn").append("answer", "burnt - burnt - brûler"));
        items.add(new BasicDBObject().append("toLearn", "burst").append("answer", "burst - burst - éclater"));
        items.add(new BasicDBObject().append("toLearn", "buy").append("answer", "bought - bought - acheter"));
        items.add(new BasicDBObject().append("toLearn", "cast").append("answer", "cast - cast - jeter, lancer"));
        items.add(new BasicDBObject().append("toLearn", "catch").append("answer", "caught - caught - attraper"));
        items.add(new BasicDBObject().append("toLearn", "choose").append("answer", "chose - chosen - choisir"));
        items.add(new BasicDBObject().append("toLearn", "cling").append("answer", "clung - clung - s'accrocher"));
        items.add(new BasicDBObject().append("toLearn", "come").append("answer", "came - come - venir"));
        items.add(new BasicDBObject().append("toLearn", "cost").append("answer", "cost - cost - coûter"));
        items.add(new BasicDBObject().append("toLearn", "creep").append("answer", "crept - crept - ramper"));
        items.add(new BasicDBObject().append("toLearn", "cut").append("answer", "cut - cut - couper"));
        items.add(new BasicDBObject().append("toLearn", "deal").append("answer", "dealt - dealt - distribuer"));
        items.add(new BasicDBObject().append("toLearn", "dig").append("answer", "dug - dug - creuser"));
        items.add(new BasicDBObject().append("toLearn", "do").append("answer", "did - done - faire"));
        items.add(new BasicDBObject().append("toLearn", "draw").append("answer", "drew - drawn - dessiner"));
        items.add(new BasicDBObject().append("toLearn", "dream").append("answer", "dreamt - dreamt - rêver"));
        items.add(new BasicDBObject().append("toLearn", "drink").append("answer", "drank - drunk - boire"));
        items.add(new BasicDBObject().append("toLearn", "drive").append("answer", "drove - driven - conduire"));
        items.add(new BasicDBObject().append("toLearn", "eat").append("answer", "ate - eaten - manger"));
        items.add(new BasicDBObject().append("toLearn", "fall").append("answer", "fell - fallen - tomber"));
        items.add(new BasicDBObject().append("toLearn", "feed").append("answer", "fed - fed - nourrir"));
        items.add(new BasicDBObject().append("toLearn", "feel").append("answer", "felt - felt - sentir, éprouver"));
        items.add(new BasicDBObject().append("toLearn", "fight").append("answer", "fought - fought - combattre"));
        items.add(new BasicDBObject().append("toLearn", "find").append("answer", "found - found - trouver"));
        items.add(new BasicDBObject().append("toLearn", "flee").append("answer", "fled - fled - s'enfuir"));
        items.add(new BasicDBObject().append("toLearn", "fly").append("answer", "flew - flown - voler"));
        items.add(new BasicDBObject().append("toLearn", "forbid").append("answer", "forbade - forbidden - interdire"));
        items.add(new BasicDBObject().append("toLearn", "forget").append("answer", "forgot - forgotten - oublier"));
        items.add(new BasicDBObject().append("toLearn", "forgive").append("answer", "forgave - forgiven - pardonner"));
        items.add(new BasicDBObject().append("toLearn", "freeze").append("answer", "froze - frozen - geler"));
        items.add(new BasicDBObject().append("toLearn", "get").append("answer", "got - got - obtenir"));
        items.add(new BasicDBObject().append("toLearn", "give").append("answer", "gave - given - donner"));
        items.add(new BasicDBObject().append("toLearn", "go").append("answer", "went - gone - aller"));
        items.add(new BasicDBObject().append("toLearn", "grind").append("answer", "ground - ground - moudre"));
        items.add(new BasicDBObject().append("toLearn", "grow").append("answer", "grew - grown - grandir"));
        items.add(new BasicDBObject().append("toLearn", "hang").append("answer", "hung - hung - pendre, accrocher"));
        items.add(new BasicDBObject().append("toLearn", "have").append("answer", "had - had - avoir"));
        items.add(new BasicDBObject().append("toLearn", "hear").append("answer", "heard - heard - entendre"));
        items.add(new BasicDBObject().append("toLearn", "hide").append("answer", "hid - hidden - (se) cacher"));
        items.add(new BasicDBObject().append("toLearn", "hit").append("answer", "hit - hit - frapper, atteindre"));
        items.add(new BasicDBObject().append("toLearn", "hold").append("answer", "held - held - tenir"));
        items.add(new BasicDBObject().append("toLearn", "hurt").append("answer", "hurt - hurt - blesser"));
        items.add(new BasicDBObject().append("toLearn", "keep").append("answer", "kept - kept - garder"));
        items.add(new BasicDBObject().append("toLearn", "kneel").append("answer", "knelt - knelt - s'agenouiller"));
        items.add(new BasicDBObject().append("toLearn", "know").append("answer", "knew - known - savoir, connaître"));
        items.add(new BasicDBObject().append("toLearn", "lay").append("answer", "laid - laid - poser à plat"));
        items.add(new BasicDBObject().append("toLearn", "lead").append("answer", "led - led - mener"));
        items.add(new BasicDBObject().append("toLearn", "lean").append("answer", "leant - leant - s'appuyer"));
        items.add(new BasicDBObject().append("toLearn", "leap").append("answer", "leapt - leapt - sauter"));
        items.add(new BasicDBObject().append("toLearn", "learn").append("answer", "learnt - learnt - apprendre"));
        items.add(new BasicDBObject().append("toLearn", "leave").append("answer", "left - left - laisser, quitter"));
        items.add(new BasicDBObject().append("toLearn", "lend").append("answer", "lent - lent - prêter"));
        items.add(new BasicDBObject().append("toLearn", "let").append("answer", "let - let - permettre, louer"));
        items.add(new BasicDBObject().append("toLearn", "lie").append("answer", "lay - lain - être étendu"));
        items.add(new BasicDBObject().append("toLearn", "light").append("answer", "lit - lit - allumer"));
        items.add(new BasicDBObject().append("toLearn", "lose").append("answer", "lost - lost - perdre"));
        items.add(new BasicDBObject().append("toLearn", "make").append("answer", "made - made - faire, fabriquer"));
        items.add(new BasicDBObject().append("toLearn", "mean").append("answer", "meant - meant - signifier"));
        items.add(new BasicDBObject().append("toLearn", "meet").append("answer", "met - met - (se) rencontrer"));
        items.add(new BasicDBObject().append("toLearn", "mow").append("answer", "mowed - mown - tondre"));
        items.add(new BasicDBObject().append("toLearn", "overcome").append("answer", "overcame - overcome - surmonter, vaincre"));
        items.add(new BasicDBObject().append("toLearn", "pay").append("answer", "paid - paid - payer"));
        items.add(new BasicDBObject().append("toLearn", "put").append("answer", "put - put - mettre"));
        items.add(new BasicDBObject().append("toLearn", "quit").append("answer", "quit - quit - cesser (de)"));
        items.add(new BasicDBObject().append("toLearn", "read").append("answer", "read - read - lire"));
        items.add(new BasicDBObject().append("toLearn", "ride").append("answer", "rode - ridden - chevaucher"));
        items.add(new BasicDBObject().append("toLearn", "ring").append("answer", "rang - rung - sonner"));
        items.add(new BasicDBObject().append("toLearn", "rise").append("answer", "rose - risen - s'élever, se lever"));
        items.add(new BasicDBObject().append("toLearn", "run").append("answer", "ran - run - courir"));
        items.add(new BasicDBObject().append("toLearn", "say").append("answer", "said - said - dire"));
        items.add(new BasicDBObject().append("toLearn", "see").append("answer", "saw - seen - voir"));
        items.add(new BasicDBObject().append("toLearn", "seek").append("answer", "sought - sought - chercher"));
        items.add(new BasicDBObject().append("toLearn", "sell").append("answer", "sold - sold - vendre"));
        items.add(new BasicDBObject().append("toLearn", "send").append("answer", "sent - sent - envoyer"));
        items.add(new BasicDBObject().append("toLearn", "set").append("answer", "set - set - fixer"));
        items.add(new BasicDBObject().append("toLearn", "shake").append("answer", "shook - shaken - secouer"));
        items.add(new BasicDBObject().append("toLearn", "shine").append("answer", "shone - shone - briller"));
        items.add(new BasicDBObject().append("toLearn", "shoot").append("answer", "shot - shot - tirer"));
        items.add(new BasicDBObject().append("toLearn", "show").append("answer", "showed - shown - montrer"));
        items.add(new BasicDBObject().append("toLearn", "shut").append("answer", "shut - shut - fermer"));
        items.add(new BasicDBObject().append("toLearn", "sing").append("answer", "sang - sung - chanter"));
        items.add(new BasicDBObject().append("toLearn", "sink").append("answer", "sank - sunk - couler"));
        items.add(new BasicDBObject().append("toLearn", "sit").append("answer", "sat - sat - être assis"));
        items.add(new BasicDBObject().append("toLearn", "sleep").append("answer", "slept - slept - dormir"));
        items.add(new BasicDBObject().append("toLearn", "slide").append("answer", "slid - slid - glisser"));
        items.add(new BasicDBObject().append("toLearn", "smell").append("answer", "smelt - smelt - sentir (odorat)"));
        items.add(new BasicDBObject().append("toLearn", "speak").append("answer", "spoke - spoken - parler"));
        items.add(new BasicDBObject().append("toLearn", "speed").append("answer", "sped - sped - aller à toute vitesse"));
        items.add(new BasicDBObject().append("toLearn", "spell").append("answer", "spelt - spelt - épeler"));
        items.add(new BasicDBObject().append("toLearn", "spend").append("answer", "spent - spent - dépenser"));
        items.add(new BasicDBObject().append("toLearn", "spill").append("answer", "spilt - spilt - renverser (un liquide)"));
        items.add(new BasicDBObject().append("toLearn", "spit").append("answer", "spat - spat - cracher"));
        items.add(new BasicDBObject().append("toLearn", "spoil").append("answer", "spoilt - spoilt - gâcher, gâter"));
        items.add(new BasicDBObject().append("toLearn", "spread").append("answer", "spread - spread - répandre"));
        items.add(new BasicDBObject().append("toLearn", "stand").append("answer", "stood - stood - être debout"));
        items.add(new BasicDBObject().append("toLearn", "steal").append("answer", "stole - stolen - voler, dérober"));
        items.add(new BasicDBObject().append("toLearn", "stick").append("answer", "stuck - stuck - coller"));
        items.add(new BasicDBObject().append("toLearn", "sting").append("answer", "stung - stung - piquer"));
        items.add(new BasicDBObject().append("toLearn", "stink").append("answer", "stank - stunk - puer"));
        items.add(new BasicDBObject().append("toLearn", "strike").append("answer", "struck - struck - frapper"));
        items.add(new BasicDBObject().append("toLearn", "strive").append("answer", "strove - striven - s'efforcer"));
        items.add(new BasicDBObject().append("toLearn", "swear").append("answer", "swore - sworn - jurer"));
        items.add(new BasicDBObject().append("toLearn", "sweep").append("answer", "swept - swept - balayer"));
        items.add(new BasicDBObject().append("toLearn", "swim").append("answer", "swam - swum - nager"));
        items.add(new BasicDBObject().append("toLearn", "swing").append("answer", "swung - swung - se balancer"));
        items.add(new BasicDBObject().append("toLearn", "take").append("answer", "took - taken - prendre"));
        items.add(new BasicDBObject().append("toLearn", "teach").append("answer", "taught - taught - enseigner"));
        items.add(new BasicDBObject().append("toLearn", "tear").append("answer", "tore - torn - déchirer"));
        items.add(new BasicDBObject().append("toLearn", "tell").append("answer", "told - told - dire, raconter"));
        items.add(new BasicDBObject().append("toLearn", "think").append("answer", "thought - thought - penser"));
        items.add(new BasicDBObject().append("toLearn", "throw").append("answer", "threw - thrown - jeter"));
        items.add(new BasicDBObject().append("toLearn", "undergo").append("answer", "underwent - undergone - subir"));
        items.add(new BasicDBObject().append("toLearn", "understand").append("answer", "understood - understood - comprendre"));
        items.add(new BasicDBObject().append("toLearn", "upset").append("answer", "upset - upset - bouleverser"));
        items.add(new BasicDBObject().append("toLearn", "wake").append("answer", "woke - woken - (se) réveiller"));
        items.add(new BasicDBObject().append("toLearn", "wear").append("answer", "wore - worn - porter (des vêtements)"));
        items.add(new BasicDBObject().append("toLearn", "weep").append("answer", "wept - wept - pleurer"));
        items.add(new BasicDBObject().append("toLearn", "win").append("answer", "won - won - gagner"));
        items.add(new BasicDBObject().append("toLearn", "withdraw").append("answer", "withdrew - withdrawn - (se) retirer"));
        items.add(new BasicDBObject().append("toLearn", "write").append("answer", "wrote - written - écrire"));

        vocaBoostListCollection.insert(
            BasicDBObjectBuilder.start()
                .add("name", "Verbes irréguliers anglais")
                .add("description", "Verbes irréguliers anglais - Niveau classe de 2nde")
                .add("language", "en")
                .add("sharingMode", "PUBLIC")
                .add("vocaBoostItems", items)
                .add("createdBy", "admin")
                .add("lastModifiedBy", "admin")
                .get());
    }

    @ChangeSet(order = "02", author = "admin", id = "English-IrregularVerbsLevel2")
    public void addEnglishIrregularVerbsListLevel2(DB db) {
        DBCollection vocaBoostListCollection = db.getCollection("vocaBoostList");

        List<BasicDBObject> items = new ArrayList<>();
        items.add(new BasicDBObject().append("toLearn", "awake").append("answer", "awoke - awoken - (se) réveiller"));
        items.add(new BasicDBObject().append("toLearn", "beat").append("answer", "beat - beaten - battre"));
        items.add(new BasicDBObject().append("toLearn", "become").append("answer", "became - become - devenir"));
        items.add(new BasicDBObject().append("toLearn", "begin").append("answer", "began - begun - commencer"));
        items.add(new BasicDBObject().append("toLearn", "bite").append("answer", "bit - bitten - mordre"));
        items.add(new BasicDBObject().append("toLearn", "blow").append("answer", "blew - blown - souffler"));
        items.add(new BasicDBObject().append("toLearn", "break").append("answer", "broke - broken - casser"));
        items.add(new BasicDBObject().append("toLearn", "bring").append("answer", "brought - brought - apporter"));
        items.add(new BasicDBObject().append("toLearn", "build").append("answer", "built - built - construire"));
        items.add(new BasicDBObject().append("toLearn", "burn").append("answer", "burnt - burnt - brûler"));
        items.add(new BasicDBObject().append("toLearn", "burst").append("answer", "burst - burst - éclater"));
        items.add(new BasicDBObject().append("toLearn", "buy").append("answer", "bought - bought - acheter"));
        items.add(new BasicDBObject().append("toLearn", "catch").append("answer", "caught - caught - attraper"));
        items.add(new BasicDBObject().append("toLearn", "choose").append("answer", "chose - chosen - choisir"));
        items.add(new BasicDBObject().append("toLearn", "come").append("answer", "came - come - venir"));
        items.add(new BasicDBObject().append("toLearn", "cost").append("answer", "cost - cost - coûter"));
        items.add(new BasicDBObject().append("toLearn", "cut").append("answer", "cut - cut - couper"));
        items.add(new BasicDBObject().append("toLearn", "dig").append("answer", "dug - dug - creuser"));
        items.add(new BasicDBObject().append("toLearn", "do").append("answer", "did - done - faire"));
        items.add(new BasicDBObject().append("toLearn", "draw").append("answer", "drew - drawn - dessiner"));
        items.add(new BasicDBObject().append("toLearn", "dream").append("answer", "dreamt - dreamt - rêver"));
        items.add(new BasicDBObject().append("toLearn", "drink").append("answer", "drank - drunk - boire"));
        items.add(new BasicDBObject().append("toLearn", "drive").append("answer", "drove - driven - conduire"));
        items.add(new BasicDBObject().append("toLearn", "eat").append("answer", "ate - eaten - manger"));
        items.add(new BasicDBObject().append("toLearn", "fall").append("answer", "fell - fallen - tomber"));
        items.add(new BasicDBObject().append("toLearn", "feed").append("answer", "fed - fed - nourrir"));
        items.add(new BasicDBObject().append("toLearn", "feel").append("answer", "felt - felt - sentir, éprouver"));
        items.add(new BasicDBObject().append("toLearn", "fight").append("answer", "fought - fought - combattre"));
        items.add(new BasicDBObject().append("toLearn", "find").append("answer", "found - found - trouver"));
        items.add(new BasicDBObject().append("toLearn", "fly").append("answer", "flew - flown - voler"));
        items.add(new BasicDBObject().append("toLearn", "forbid").append("answer", "forbade - forbidden - interdire"));
        items.add(new BasicDBObject().append("toLearn", "forget").append("answer", "forgot - forgotten - oublier"));
        items.add(new BasicDBObject().append("toLearn", "forgive").append("answer", "forgave - forgiven - pardonner"));
        items.add(new BasicDBObject().append("toLearn", "freeze").append("answer", "froze - frozen - geler"));
        items.add(new BasicDBObject().append("toLearn", "get").append("answer", "got - got - obtenir"));
        items.add(new BasicDBObject().append("toLearn", "give").append("answer", "gave - given - donner"));
        items.add(new BasicDBObject().append("toLearn", "go").append("answer", "went - gone - aller"));
        items.add(new BasicDBObject().append("toLearn", "grind").append("answer", "ground - ground - moudre"));
        items.add(new BasicDBObject().append("toLearn", "grow").append("answer", "grew - grown - grandir"));
        items.add(new BasicDBObject().append("toLearn", "hang").append("answer", "hung - hung - pendre, accrocher"));
        items.add(new BasicDBObject().append("toLearn", "have").append("answer", "had - had - avoir"));
        items.add(new BasicDBObject().append("toLearn", "hear").append("answer", "heard - heard - entendre"));
        items.add(new BasicDBObject().append("toLearn", "hide").append("answer", "hid - hidden - (se) cacher"));
        items.add(new BasicDBObject().append("toLearn", "hit").append("answer", "hit - hit - frapper, atteindre"));
        items.add(new BasicDBObject().append("toLearn", "hold").append("answer", "held - held - tenir"));
        items.add(new BasicDBObject().append("toLearn", "hurt").append("answer", "hurt - hurt - blesser"));
        items.add(new BasicDBObject().append("toLearn", "keep").append("answer", "kept - kept - garder"));
        items.add(new BasicDBObject().append("toLearn", "kneel").append("answer", "knelt - knelt - s'agenouiller"));
        items.add(new BasicDBObject().append("toLearn", "know").append("answer", "knew - known - savoir, connaître"));
        items.add(new BasicDBObject().append("toLearn", "lay").append("answer", "laid - laid - poser à plat"));
        items.add(new BasicDBObject().append("toLearn", "lead").append("answer", "led - led - mener"));
        items.add(new BasicDBObject().append("toLearn", "learn").append("answer", "learnt - learnt - apprendre"));
        items.add(new BasicDBObject().append("toLearn", "leave").append("answer", "left - left - laisser, quitter"));
        items.add(new BasicDBObject().append("toLearn", "lend").append("answer", "lent - lent - prêter"));
        items.add(new BasicDBObject().append("toLearn", "let").append("answer", "let - let - permettre, louer"));
        items.add(new BasicDBObject().append("toLearn", "lie").append("answer", "lay - lain - être étendu"));
        items.add(new BasicDBObject().append("toLearn", "light").append("answer", "lit - lit - allumer"));
        items.add(new BasicDBObject().append("toLearn", "lose").append("answer", "lost - lost - perdre"));
        items.add(new BasicDBObject().append("toLearn", "make").append("answer", "made - made - faire, fabriquer"));
        items.add(new BasicDBObject().append("toLearn", "mean").append("answer", "meant - meant - signifier"));
        items.add(new BasicDBObject().append("toLearn", "meet").append("answer", "met - met - (se) rencontrer"));
        items.add(new BasicDBObject().append("toLearn", "mow").append("answer", "mowed - mown - tondre"));
        items.add(new BasicDBObject().append("toLearn", "pay").append("answer", "paid - paid - payer"));
        items.add(new BasicDBObject().append("toLearn", "put").append("answer", "put - put - mettre"));
        items.add(new BasicDBObject().append("toLearn", "read").append("answer", "read - read - lire"));
        items.add(new BasicDBObject().append("toLearn", "ride").append("answer", "rode - ridden - chevaucher"));
        items.add(new BasicDBObject().append("toLearn", "ring").append("answer", "rang - rung - sonner"));
        items.add(new BasicDBObject().append("toLearn", "rise").append("answer", "rose - risen - s'élever, se lever"));
        items.add(new BasicDBObject().append("toLearn", "run").append("answer", "ran - run - courir"));
        items.add(new BasicDBObject().append("toLearn", "say").append("answer", "said - said - dire"));
        items.add(new BasicDBObject().append("toLearn", "see").append("answer", "saw - seen - voir"));
        items.add(new BasicDBObject().append("toLearn", "seek").append("answer", "sought - sought - chercher"));
        items.add(new BasicDBObject().append("toLearn", "sell").append("answer", "sold - sold - vendre"));
        items.add(new BasicDBObject().append("toLearn", "send").append("answer", "sent - sent - envoyer"));
        items.add(new BasicDBObject().append("toLearn", "set").append("answer", "set - set - fixer"));
        items.add(new BasicDBObject().append("toLearn", "shake").append("answer", "shook - shaken - secouer"));
        items.add(new BasicDBObject().append("toLearn", "shoot").append("answer", "shot - shot - tirer"));
        items.add(new BasicDBObject().append("toLearn", "show").append("answer", "showed - shown - montrer"));
        items.add(new BasicDBObject().append("toLearn", "shut").append("answer", "shut - shut - fermer"));
        items.add(new BasicDBObject().append("toLearn", "sing").append("answer", "sang - sung - chanter"));
        items.add(new BasicDBObject().append("toLearn", "sit").append("answer", "sat - sat - être assis"));
        items.add(new BasicDBObject().append("toLearn", "sleep").append("answer", "slept - slept - dormir"));
        items.add(new BasicDBObject().append("toLearn", "smell").append("answer", "smelt - smelt - sentir (odorat)"));
        items.add(new BasicDBObject().append("toLearn", "speak").append("answer", "spoke - spoken - parler"));
        items.add(new BasicDBObject().append("toLearn", "spell").append("answer", "spelt - spelt - épeler"));
        items.add(new BasicDBObject().append("toLearn", "spend").append("answer", "spent - spent - dépenser"));
        items.add(new BasicDBObject().append("toLearn", "spit").append("answer", "spat - spat - cracher"));
        items.add(new BasicDBObject().append("toLearn", "spoil").append("answer", "spoilt - spoilt - gâcher, gâter"));
        items.add(new BasicDBObject().append("toLearn", "spread").append("answer", "spread - spread - répandre"));
        items.add(new BasicDBObject().append("toLearn", "stand").append("answer", "stood - stood - être debout"));
        items.add(new BasicDBObject().append("toLearn", "steal").append("answer", "stole - stolen - voler, dérober"));
        items.add(new BasicDBObject().append("toLearn", "stick").append("answer", "stuck - stuck - coller"));
        items.add(new BasicDBObject().append("toLearn", "sting").append("answer", "stung - stung - piquer"));
        items.add(new BasicDBObject().append("toLearn", "stink").append("answer", "stank - stunk - puer"));
        items.add(new BasicDBObject().append("toLearn", "strike").append("answer", "struck - struck - frapper"));
        items.add(new BasicDBObject().append("toLearn", "swear").append("answer", "swore - sworn - jurer"));
        items.add(new BasicDBObject().append("toLearn", "sweep").append("answer", "swept - swept - balayer"));
        items.add(new BasicDBObject().append("toLearn", "swim").append("answer", "swam - swum - nager"));
        items.add(new BasicDBObject().append("toLearn", "take").append("answer", "took - taken - prendre"));
        items.add(new BasicDBObject().append("toLearn", "teach").append("answer", "taught - taught - enseigner"));
        items.add(new BasicDBObject().append("toLearn", "tear").append("answer", "tore - torn - déchirer"));
        items.add(new BasicDBObject().append("toLearn", "tell").append("answer", "told - told - dire, raconter"));
        items.add(new BasicDBObject().append("toLearn", "think").append("answer", "thought - thought - penser"));
        items.add(new BasicDBObject().append("toLearn", "throw").append("answer", "threw - thrown - jeter"));
        items.add(new BasicDBObject().append("toLearn", "understand").append("answer", "understood - understood - comprendre"));
        items.add(new BasicDBObject().append("toLearn", "upset").append("answer", "upset - upset - bouleverser"));
        items.add(new BasicDBObject().append("toLearn", "wake").append("answer", "woke - woken - (se) réveiller"));
        items.add(new BasicDBObject().append("toLearn", "wear").append("answer", "wore - worn - porter (des vêtements)"));
        items.add(new BasicDBObject().append("toLearn", "weep").append("answer", "wept - wept - pleurer"));
        items.add(new BasicDBObject().append("toLearn", "win").append("answer", "won - won - gagner"));
        items.add(new BasicDBObject().append("toLearn", "withdraw").append("answer", "withdrew - withdrawn - (se) retirer"));
        items.add(new BasicDBObject().append("toLearn", "write").append("answer", "wrote - written - écrire"));

        vocaBoostListCollection.insert(
            BasicDBObjectBuilder.start()
                .add("name", "Verbes irréguliers anglais")
                .add("description", "Verbes irréguliers anglais - Niveau classe de 3ème")
                .add("language", "en")
                .add("sharingMode", "PUBLIC")
                .add("vocaBoostItems", items)
                .add("createdBy", "admin")
                .add("lastModifiedBy", "admin")
                .get());
    }

    @ChangeSet(order = "03", author = "admin", id = "English-IrregularVerbsLevel1")
    public void addEnglishIrregularVerbsListLevel1(DB db) {
        DBCollection vocaBoostListCollection = db.getCollection("vocaBoostList");

        List<BasicDBObject> items = new ArrayList<>();
        items.add(new BasicDBObject().append("toLearn", "beat").append("answer", "beat - beaten - battre"));
        items.add(new BasicDBObject().append("toLearn", "become").append("answer", "became - become - devenir"));
        items.add(new BasicDBObject().append("toLearn", "begin").append("answer", "began - begun - commencer"));
        items.add(new BasicDBObject().append("toLearn", "blow").append("answer", "blew - blown - souffler"));
        items.add(new BasicDBObject().append("toLearn", "break").append("answer", "broke - broken - casser"));
        items.add(new BasicDBObject().append("toLearn", "bring").append("answer", "brought - brought - apporter"));
        items.add(new BasicDBObject().append("toLearn", "build").append("answer", "built - built - construire"));
        items.add(new BasicDBObject().append("toLearn", "burn").append("answer", "burnt - burnt - brûler"));
        items.add(new BasicDBObject().append("toLearn", "buy").append("answer", "bought - bought - acheter"));
        items.add(new BasicDBObject().append("toLearn", "catch").append("answer", "caught - caught - attraper"));
        items.add(new BasicDBObject().append("toLearn", "choose").append("answer", "chose - chosen - choisir"));
        items.add(new BasicDBObject().append("toLearn", "come").append("answer", "came - come - venir"));
        items.add(new BasicDBObject().append("toLearn", "cost").append("answer", "cost - cost - coûter"));
        items.add(new BasicDBObject().append("toLearn", "cut").append("answer", "cut - cut - couper"));
        items.add(new BasicDBObject().append("toLearn", "dig").append("answer", "dug - dug - creuser"));
        items.add(new BasicDBObject().append("toLearn", "do").append("answer", "did - done - faire"));
        items.add(new BasicDBObject().append("toLearn", "draw").append("answer", "drew - drawn - dessiner"));
        items.add(new BasicDBObject().append("toLearn", "dream").append("answer", "dreamt - dreamt - rêver"));
        items.add(new BasicDBObject().append("toLearn", "drink").append("answer", "drank - drunk - boire"));
        items.add(new BasicDBObject().append("toLearn", "drive").append("answer", "drove - driven - conduire"));
        items.add(new BasicDBObject().append("toLearn", "eat").append("answer", "ate - eaten - manger"));
        items.add(new BasicDBObject().append("toLearn", "fall").append("answer", "fell - fallen - tomber"));
        items.add(new BasicDBObject().append("toLearn", "feed").append("answer", "fed - fed - nourrir"));
        items.add(new BasicDBObject().append("toLearn", "feel").append("answer", "felt - felt - sentir, éprouver"));
        items.add(new BasicDBObject().append("toLearn", "fight").append("answer", "fought - fought - combattre"));
        items.add(new BasicDBObject().append("toLearn", "find").append("answer", "found - found - trouver"));
        items.add(new BasicDBObject().append("toLearn", "fly").append("answer", "flew - flown - voler"));
        items.add(new BasicDBObject().append("toLearn", "forget").append("answer", "forgot - forgotten - oublier"));
        items.add(new BasicDBObject().append("toLearn", "freeze").append("answer", "froze - frozen - geler"));
        items.add(new BasicDBObject().append("toLearn", "get").append("answer", "got - got - obtenir"));
        items.add(new BasicDBObject().append("toLearn", "give").append("answer", "gave - given - donner"));
        items.add(new BasicDBObject().append("toLearn", "go").append("answer", "went - gone - aller"));
        items.add(new BasicDBObject().append("toLearn", "grow").append("answer", "grew - grown - grandir"));
        items.add(new BasicDBObject().append("toLearn", "have").append("answer", "had - had - avoir"));
        items.add(new BasicDBObject().append("toLearn", "hear").append("answer", "heard - heard - entendre"));
        items.add(new BasicDBObject().append("toLearn", "hide").append("answer", "hid - hidden - (se) cacher"));
        items.add(new BasicDBObject().append("toLearn", "hit").append("answer", "hit - hit - frapper, atteindre"));
        items.add(new BasicDBObject().append("toLearn", "hold").append("answer", "held - held - tenir"));
        items.add(new BasicDBObject().append("toLearn", "hurt").append("answer", "hurt - hurt - blesser"));
        items.add(new BasicDBObject().append("toLearn", "keep").append("answer", "kept - kept - garder"));
        items.add(new BasicDBObject().append("toLearn", "know").append("answer", "knew - known - savoir, connaître"));
        items.add(new BasicDBObject().append("toLearn", "learn").append("answer", "learnt - learnt - apprendre"));
        items.add(new BasicDBObject().append("toLearn", "leave").append("answer", "left - left - laisser, quitter"));
        items.add(new BasicDBObject().append("toLearn", "lend").append("answer", "lent - lent - prêter"));
        items.add(new BasicDBObject().append("toLearn", "lose").append("answer", "lost - lost - perdre"));
        items.add(new BasicDBObject().append("toLearn", "make").append("answer", "made - made - faire, fabriquer"));
        items.add(new BasicDBObject().append("toLearn", "mean").append("answer", "meant - meant - signifier"));
        items.add(new BasicDBObject().append("toLearn", "meet").append("answer", "met - met - (se) rencontrer"));
        items.add(new BasicDBObject().append("toLearn", "mow").append("answer", "mowed - mown - tondre"));
        items.add(new BasicDBObject().append("toLearn", "pay").append("answer", "paid - paid - payer"));
        items.add(new BasicDBObject().append("toLearn", "put").append("answer", "put - put - mettre"));
        items.add(new BasicDBObject().append("toLearn", "read").append("answer", "read - read - lire"));
        items.add(new BasicDBObject().append("toLearn", "ride").append("answer", "rode - ridden - chevaucher"));
        items.add(new BasicDBObject().append("toLearn", "ring").append("answer", "rang - rung - sonner"));
        items.add(new BasicDBObject().append("toLearn", "rise").append("answer", "rose - risen - s'élever, se lever"));
        items.add(new BasicDBObject().append("toLearn", "run").append("answer", "ran - run - courir"));
        items.add(new BasicDBObject().append("toLearn", "say").append("answer", "said - said - dire"));
        items.add(new BasicDBObject().append("toLearn", "see").append("answer", "saw - seen - voir"));
        items.add(new BasicDBObject().append("toLearn", "sell").append("answer", "sold - sold - vendre"));
        items.add(new BasicDBObject().append("toLearn", "send").append("answer", "sent - sent - envoyer"));
        items.add(new BasicDBObject().append("toLearn", "shake").append("answer", "shook - shaken - secouer"));
        items.add(new BasicDBObject().append("toLearn", "show").append("answer", "showed - shown - montrer"));
        items.add(new BasicDBObject().append("toLearn", "sing").append("answer", "sang - sung - chanter"));
        items.add(new BasicDBObject().append("toLearn", "sit").append("answer", "sat - sat - être assis"));
        items.add(new BasicDBObject().append("toLearn", "sleep").append("answer", "slept - slept - dormir"));
        items.add(new BasicDBObject().append("toLearn", "smell").append("answer", "smelt - smelt - sentir (odorat)"));
        items.add(new BasicDBObject().append("toLearn", "speak").append("answer", "spoke - spoken - parler"));
        items.add(new BasicDBObject().append("toLearn", "spend").append("answer", "spent - spent - dépenser"));
        items.add(new BasicDBObject().append("toLearn", "stand").append("answer", "stood - stood - être debout"));
        items.add(new BasicDBObject().append("toLearn", "steal").append("answer", "stole - stolen - voler, dérober"));
        items.add(new BasicDBObject().append("toLearn", "swear").append("answer", "swore - sworn - jurer"));
        items.add(new BasicDBObject().append("toLearn", "sweep").append("answer", "swept - swept - balayer"));
        items.add(new BasicDBObject().append("toLearn", "swim").append("answer", "swam - swum - nager"));
        items.add(new BasicDBObject().append("toLearn", "take").append("answer", "took - taken - prendre"));
        items.add(new BasicDBObject().append("toLearn", "teach").append("answer", "taught - taught - enseigner"));
        items.add(new BasicDBObject().append("toLearn", "tell").append("answer", "told - told - dire, raconter"));
        items.add(new BasicDBObject().append("toLearn", "think").append("answer", "thought - thought - penser"));
        items.add(new BasicDBObject().append("toLearn", "throw").append("answer", "threw - thrown - jeter"));
        items.add(new BasicDBObject().append("toLearn", "understand").append("answer", "understood - understood - comprendre"));
        items.add(new BasicDBObject().append("toLearn", "wake").append("answer", "woke - woken - (se) réveiller"));
        items.add(new BasicDBObject().append("toLearn", "wear").append("answer", "wore - worn - porter (des vêtements)"));
        items.add(new BasicDBObject().append("toLearn", "win").append("answer", "won - won - gagner"));
        items.add(new BasicDBObject().append("toLearn", "write").append("answer", "wrote - written - écrire"));

        vocaBoostListCollection.insert(
            BasicDBObjectBuilder.start()
                .add("name", "Verbes irréguliers anglais")
                .add("description", "Verbes irréguliers anglais - Niveau classe de 4ème")
                .add("language", "en")
                .add("sharingMode", "PUBLIC")
                .add("vocaBoostItems", items)
                .add("createdBy", "admin")
                .add("lastModifiedBy", "admin")
                .get());
    }

}
