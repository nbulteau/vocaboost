package fr.nbu.vocaboost.config.dbmigrations;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates old vocaboost lists
 */
@ChangeLog(order = "004")
public class ItalianVocabulary {

    @ChangeSet(order = "01", author = "admin", id = "Italian-Table")
    public void addTableVocalulary(DB db) {
        DBCollection vocaBoostListCollection = db.getCollection("vocaBoostList");

        List<BasicDBObject> items = new ArrayList<>();
        items.add(new BasicDBObject().append("toLearn", "burro").append("answer", "beurre"));
        items.add(new BasicDBObject().append("toLearn", "acqua").append("answer", "eau"));
        items.add(new BasicDBObject().append("toLearn", "spezie").append("answer", "épice"));
        items.add(new BasicDBObject().append("toLearn", "piccante").append("answer", "épicé"));
        items.add(new BasicDBObject().append("toLearn", "friggere").append("answer", "frire"));
        items.add(new BasicDBObject().append("toLearn", "il cibo").append("answer", "la nourriture"));
        items.add(new BasicDBObject().append("toLearn", "la tavola").append("answer", "la table"));
        items.add(new BasicDBObject().append("toLearn", "latte").append("answer", "lait"));
        items.add(new BasicDBObject().append("toLearn", "il pranzo").append("answer", "le déjeuner"));
        items.add(new BasicDBObject().append("toLearn", "dolce e frutta").append("answer", "le déssert"));
        items.add(new BasicDBObject().append("toLearn", "la cena").append("answer", "le diner"));
        items.add(new BasicDBObject().append("toLearn", "il formaggio").append("answer", "le fromage"));
        items.add(new BasicDBObject().append("toLearn", "la merenda").append("answer", "le goûter"));
        items.add(new BasicDBObject().append("toLearn", "il pane").append("answer", "le pain"));
        items.add(new BasicDBObject().append("toLearn", "la colazione").append("answer", "le petit déjeuner"));
        items.add(new BasicDBObject().append("toLearn", "il piatto caldo").append("answer", "le plat de résistance"));
        items.add(new BasicDBObject().append("toLearn", "il carrello a formaggi").append("answer", "le plateau à fromages"));
        items.add(new BasicDBObject().append("toLearn", "l'antipasto").append("answer", "l'entrée"));
        items.add(new BasicDBObject().append("toLearn", "pepe").append("answer", "poivre"));
        items.add(new BasicDBObject().append("toLearn", "prodotto lattiero").append("answer", "produit laitier"));
        items.add(new BasicDBObject().append("toLearn", "salato").append("answer", "salé"));
        items.add(new BasicDBObject().append("toLearn", "sale").append("answer", "sel"));
        items.add(new BasicDBObject().append("toLearn", "un coltello").append("answer", "un couteau"));
        items.add(new BasicDBObject().append("toLearn", "un pasto").append("answer", "un repas"));
        items.add(new BasicDBObject().append("toLearn", "un ristorante").append("answer", "un restaurant"));
        items.add(new BasicDBObject().append("toLearn", "un bicchiere").append("answer", "un verre"));
        items.add(new BasicDBObject().append("toLearn", "una caraffa").append("answer", "une carafe"));
        items.add(new BasicDBObject().append("toLearn", "una pentola").append("answer", "une casserole"));
        items.add(new BasicDBObject().append("toLearn", "un cucchiaio da zuppa").append("answer", "une cuillère à soupe"));
        items.add(new BasicDBObject().append("toLearn", "una forchetta").append("answer", "une fourchette"));
        items.add(new BasicDBObject().append("toLearn", "una macchina a caffè").append("answer", "une machine à café"));
        items.add(new BasicDBObject().append("toLearn", "una tovaglia").append("answer", "une nappe"));
        items.add(new BasicDBObject().append("toLearn", "un cucchiaino").append("answer", "une petite cuillère"));
        items.add(new BasicDBObject().append("toLearn", "una padella").append("answer", "une poêle"));
        items.add(new BasicDBObject().append("toLearn", "una zuppa").append("answer", "une soupe"));
        items.add(new BasicDBObject().append("toLearn", "una macchia").append("answer", "une tâche"));
        items.add(new BasicDBObject().append("toLearn", "masserizie").append("answer", "ustensile"));
        items.add(new BasicDBObject().append("toLearn", "il carne").append("answer", "viande"));
        items.add(new BasicDBObject().append("toLearn", "il vino").append("answer", "vin"));

        vocaBoostListCollection.insert(
            BasicDBObjectBuilder.start()
                .add("name", "Vocabulaire Italien")
                .add("description", "Vocabulaire sélectif à connaître : A table")
                .add("language", "it")
                .add("sharingMode", "PUBLIC")
                .add("vocaBoostItems", items)
                .add("createdBy", "admin")
                .add("lastModifiedBy", "admin")
                .get());
    }

    @ChangeSet(order = "02", author = "admin", id = "Italian-Dressing")
    public void addDressingVocalulary(DB db) {
        DBCollection vocaBoostListCollection = db.getCollection("vocaBoostList");

        List<BasicDBObject> items = new ArrayList<>();
        items.add(new BasicDBObject().append("toLearn", "calze").append("answer", "des chaussettes"));
        items.add(new BasicDBObject().append("toLearn", "camicetta").append("answer", "un chemisier"));
        items.add(new BasicDBObject().append("toLearn", "chic").append("answer", "chic"));
        items.add(new BasicDBObject().append("toLearn", "comodo").append("answer", "confortable"));
        items.add(new BasicDBObject().append("toLearn", "elegante").append("answer", "élégant"));
        items.add(new BasicDBObject().append("toLearn", "guanti").append("answer", "des gants"));
        items.add(new BasicDBObject().append("toLearn", "jeans").append("answer", "un jean"));
        items.add(new BasicDBObject().append("toLearn", "occhiali").append("answer", "des lunettes"));
        items.add(new BasicDBObject().append("toLearn", "pantaloncini").append("answer", "un short"));
        items.add(new BasicDBObject().append("toLearn", "pantaloni").append("answer", "un pantalon"));
        items.add(new BasicDBObject().append("toLearn", "pulito").append("answer", "propre"));
        items.add(new BasicDBObject().append("toLearn", "scarpe").append("answer", "des chaussures"));
        items.add(new BasicDBObject().append("toLearn", "sporco").append("answer", "sale"));
        items.add(new BasicDBObject().append("toLearn", "stivali").append("answer", "des bottes"));
        items.add(new BasicDBObject().append("toLearn", "striato").append("answer", "rayé"));
        items.add(new BasicDBObject().append("toLearn", "un anello").append("answer", "une bague"));
        items.add(new BasicDBObject().append("toLearn", "un cappello").append("answer", "un chapeau"));
        items.add(new BasicDBObject().append("toLearn", "un cappotto").append("answer", "un manteau"));
        items.add(new BasicDBObject().append("toLearn", "un completo").append("answer", "un complet"));
        items.add(new BasicDBObject().append("toLearn", "un costume da bagno").append("answer", "un maillot de bain"));
        items.add(new BasicDBObject().append("toLearn", "un cravattino").append("answer", "un noeud papillon"));
        items.add(new BasicDBObject().append("toLearn", "un gioiello").append("answer", "un bijou"));
        items.add(new BasicDBObject().append("toLearn", "un impermeabile").append("answer", "un imperméable"));
        items.add(new BasicDBObject().append("toLearn", "un maglione").append("answer", "un pull-over"));
        items.add(new BasicDBObject().append("toLearn", "un orecchino").append("answer", "une boucle d`oreilles"));
        items.add(new BasicDBObject().append("toLearn", "un orologio").append("answer", "une montre"));
        items.add(new BasicDBObject().append("toLearn", "un orologio").append("answer", "une montre"));
        items.add(new BasicDBObject().append("toLearn", "un paio").append("answer", "une paire"));
        items.add(new BasicDBObject().append("toLearn", "un vestito").append("answer", "une robe"));
        items.add(new BasicDBObject().append("toLearn", "una camicia").append("answer", "une chemise"));
        items.add(new BasicDBObject().append("toLearn", "una cintura").append("answer", "une ceinture"));
        items.add(new BasicDBObject().append("toLearn", "una cravatta").append("answer", "une cravate"));
        items.add(new BasicDBObject().append("toLearn", "una giacca").append("answer", "une veste"));
        items.add(new BasicDBObject().append("toLearn", "una gonna").append("answer", "une jupe"));
        items.add(new BasicDBObject().append("toLearn", "una macchia").append("answer", "une tache"));
        items.add(new BasicDBObject().append("toLearn", "una maglietta").append("answer", "un tee-shirt"));
        items.add(new BasicDBObject().append("toLearn", "una sciarpa").append("answer", "un foulard"));
        items.add(new BasicDBObject().append("toLearn", "una striscia").append("answer", "une raie"));
        items.add(new BasicDBObject().append("toLearn", "una tasca").append("answer", "une poche"));
        items.add(new BasicDBObject().append("toLearn", "una valigia").append("answer", "une valise"));
        items.add(new BasicDBObject().append("toLearn", "uno stivale").append("answer", "une botte"));
        items.add(new BasicDBObject().append("toLearn", "vestiti").append("answer", "des vêtements"));

        vocaBoostListCollection.insert(
            BasicDBObjectBuilder.start()
                .add("name", "Vocabulaire Italien")
                .add("description", "Vocabulaire sélectif à connaître : Abbigliamento - Habillement")
                .add("language", "it")
                .add("sharingMode", "PUBLIC")
                .add("vocaBoostItems", items)
                .add("createdBy", "admin")
                .add("lastModifiedBy", "admin")
                .get());
    }
}
