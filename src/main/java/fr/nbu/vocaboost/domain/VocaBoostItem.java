package fr.nbu.vocaboost.domain;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class VocaBoostItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NonNull
    private String toLearn;

    @NonNull
    private String answer;

    public void updateId() {
        if (null == id) {
            id = new org.bson.types.ObjectId().toHexString();
        } else if (id.startsWith("newId")) {
            id = new org.bson.types.ObjectId().toHexString();
        }
    }

    public String[] toArrayOfStrings() {
        return new String[] { toLearn, answer };
    }

}
