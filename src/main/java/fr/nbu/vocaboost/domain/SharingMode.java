package fr.nbu.vocaboost.domain;

public enum SharingMode {
    PUBLIC, PRIVATE, WITH_FRIENDS;
}
