package fr.nbu.vocaboost.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import fr.nbu.vocaboost.domain.util.CustomDateTimeDeserializer;
import fr.nbu.vocaboost.domain.util.CustomDateTimeSerializer;

/**
 * A VocaBoostList : List of words/verbs to learn.
 */
@Document
@Data
@NoArgsConstructor
public class VocaBoostList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Size(min = 1, max = 100)
    private String name;

    private String description;

    private Locale language;

    private SharingMode sharingMode;

    private Set<VocaBoostItem> vocaBoostItems = new HashSet<>();

    @CreatedBy
    private String createdBy;

    @CreatedDate
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private DateTime createdDate = DateTime.now();

    @LastModifiedBy
    private String lastModifiedBy;

    @LastModifiedDate
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private DateTime lastModifiedDate = DateTime.now();

    public String[] toArrayOfStrings() {
        return new String[] { name, description, language.getDisplayName(), sharingMode.name() };
    }
}
