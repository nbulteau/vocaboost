package fr.nbu.vocaboost.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import fr.nbu.vocaboost.domain.SharingMode;
import fr.nbu.vocaboost.domain.VocaBoostList;

/**
 * Spring Data MongoDB repository for the VocaBoostList entity.
 */
public interface VocaBoostListRepository extends MongoRepository<VocaBoostList, String> {

    Page<VocaBoostList> findByCreatedByOrSharingMode(String createdBy, SharingMode sharingMode, Pageable pageable);

}
