package fr.nbu.vocaboost.service;

public class NotOwnerException extends Exception {

    private static final long serialVersionUID = 1L;

    public NotOwnerException() {
        super();
    }

    public NotOwnerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public NotOwnerException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotOwnerException(String message) {
        super(message);
    }

    public NotOwnerException(Throwable cause) {
        super(cause);
    }
}
