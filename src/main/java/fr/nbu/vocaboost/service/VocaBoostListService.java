package fr.nbu.vocaboost.service;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import au.com.bytecode.opencsv.CSVWriter;
import fr.nbu.vocaboost.domain.SharingMode;
import fr.nbu.vocaboost.domain.VocaBoostItem;
import fr.nbu.vocaboost.domain.VocaBoostList;
import fr.nbu.vocaboost.repository.VocaBoostListRepository;
import fr.nbu.vocaboost.security.SecurityUtils;

@Service
public class VocaBoostListService {

    @Inject
    private VocaBoostListRepository vocaBoostListRepository;

    /**
     * Save or update the vocaBoostList.
     * 
     * @param user
     * @param vocaBoostList
     * @return
     */
    public VocaBoostList save(VocaBoostList vocaBoostList) {
        String user = SecurityUtils.getCurrentLogin();

        // Set the missing ids
        updateIds(vocaBoostList);

        vocaBoostList.setLastModifiedBy(user);
        vocaBoostList.setLastModifiedDate(DateTime.now());
        vocaBoostList.setCreatedBy(user);

        return vocaBoostListRepository.save(vocaBoostList);
    }

    /**
     * Find all the list a user can learn paginated.
     * 
     * @param user
     * @param pageable
     * @return
     */
    public Page<VocaBoostList> findAll(Pageable pageable) {
        String user = SecurityUtils.getCurrentLogin();

        return vocaBoostListRepository.findByCreatedByOrSharingMode(user, SharingMode.PUBLIC, pageable);
    }

    /**
     * Find a list by it's id.
     * 
     * @param user
     * @param id
     * @return
     * @throws NotOwnerEception
     */
    public VocaBoostList findOne(String id) throws NotOwnerException {
        VocaBoostList vocaBoostList = vocaBoostListRepository.findOne(id);

        // Does the current user own that list ?
        String user = SecurityUtils.getCurrentLogin();
        if (null != vocaBoostList) {
            if (vocaBoostList.getSharingMode() != SharingMode.PUBLIC && !vocaBoostList.getCreatedBy().equals(user)) {
                throw new NotOwnerException("User " + user + " is not the owner of the VocaBoostList : " + vocaBoostList.getName());
            }
        }
        return vocaBoostList;
    }

    /**
     * Delete a list.
     * 
     * @param user
     * @param id
     * @throws NotOwnerEception
     */
    public void delete(String id) throws NotOwnerException {
        VocaBoostList vocaBoostList = vocaBoostListRepository.findOne(id);

        // Does the current user own that list ?
        String user = SecurityUtils.getCurrentLogin();
        if (null != vocaBoostList && !vocaBoostList.getCreatedBy().equals(user)) {
            throw new NotOwnerException("User " + user + " is not the owner of the VocaBoostList : " + vocaBoostList.getName());
        } else {
            vocaBoostListRepository.delete(id);
        }
    }

    /**
     * Export a list in a CSV format.
     * 
     * @param user
     * @param id
     * @return
     * @throws IOException
     * @throws NotOwnerEception
     */
    public String exportCSV(String id) throws NotOwnerException, IOException {
        String csvString = null;

        VocaBoostList vocaBoostList = vocaBoostListRepository.findOne(id);

        // Does the current user own that list ?
        String user = SecurityUtils.getCurrentLogin();
        if (null != vocaBoostList && !vocaBoostList.getCreatedBy().equals(user)) {
            throw new NotOwnerException("User " + user + " is not the owner of the VocaBoostList : " + vocaBoostList.getName());
        }

        try (Writer writer = new StringWriter(); CSVWriter csvWriter = new CSVWriter(writer, ';')) {
            for (VocaBoostItem vocaBoostItem : vocaBoostList.getVocaBoostItems()) {
                String[] vocaBoostItemLine = vocaBoostItem.toArrayOfStrings();
                csvWriter.writeNext(vocaBoostItemLine);
            }
            csvString = writer.toString();
        }

        return csvString;
    }

    /**
     * Update vocaBoostItem ids
     *
     * @param vocaBoostList
     */
    private void updateIds(VocaBoostList vocaBoostList) {
        for (VocaBoostItem vocaBoostItem : vocaBoostList.getVocaBoostItems()) {
            vocaBoostItem.updateId();
        }
    }
}
