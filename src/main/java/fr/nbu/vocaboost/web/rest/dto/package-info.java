/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package fr.nbu.vocaboost.web.rest.dto;
