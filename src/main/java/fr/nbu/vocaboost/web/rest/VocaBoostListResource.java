package fr.nbu.vocaboost.web.rest;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriTemplate;

import com.codahale.metrics.annotation.Timed;

import fr.nbu.vocaboost.domain.VocaBoostList;
import fr.nbu.vocaboost.service.NotOwnerException;
import fr.nbu.vocaboost.service.VocaBoostListService;

/**
 * REST controller for managing VocaBoostList.
 */
@RestController
@RequestMapping("/api/v1")
public class VocaBoostListResource {

    private final Logger log = LoggerFactory.getLogger(VocaBoostListResource.class);

    private static final Logger LOGGER = LoggerFactory.getLogger("UserAction");

    @Inject
    private VocaBoostListService vocaBoostListService;

    /**
     * POST /vocaBoostLists -> Create a new vocaBoostList.
     */
    @RequestMapping(value = "/vocaBoostLists", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VocaBoostList> create(@RequestBody VocaBoostList vocaBoostList) {
        LOGGER.debug("REST request to save VocaBoostList : {}", vocaBoostList);
        vocaBoostList = vocaBoostListService.save(vocaBoostList);

        // HttpHeader : location for the POST
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new UriTemplate("/vocaBoostLists/{id}").expand(vocaBoostList.getId()));

        ResponseEntity<VocaBoostList> responseEntity = new ResponseEntity<>(headers, HttpStatus.CREATED);

        return responseEntity;
    }

    /**
     * GET /vocaBoostLists -> get all the vocaBoostLists.
     * 
     * HttpHeader : Content-Range: offset-limit/count.
     * offset = index of the first element returned
     * limit = index of the last element returned
     * count = total elements of the collection
     * 
     * HttpHeader : Accept-Range: resource max.
     * resource = vocaBoostList
     * max = max number elements taht can be request
     * 
     */
    @RequestMapping(value = "/vocaBoostLists", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<VocaBoostList>> getAll(@RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @RequestParam(value = "size", required = false, defaultValue = "50") int size) {
        LOGGER.debug("REST request to get all VocaBoostLists");
        Page<VocaBoostList> vocaBoostPage = vocaBoostListService.findAll(new PageRequest(page, size, Direction.ASC, "createdDate"));

        HttpHeaders headers = generatePaginatedGetHeaders(vocaBoostPage);

        HttpStatus httpStatus = generatePaginatedGetHttpStatus(vocaBoostPage);

        ResponseEntity<List<VocaBoostList>> responseEntity = new ResponseEntity<>(vocaBoostPage.getContent(), headers, httpStatus);

        return responseEntity;
    }

    private HttpStatus generatePaginatedGetHttpStatus(Page<VocaBoostList> vocaBoostPage) {
        HttpStatus httpStatus = HttpStatus.PARTIAL_CONTENT;
        if (vocaBoostPage.getNumberOfElements() == vocaBoostPage.getTotalElements()) {
            httpStatus = HttpStatus.OK;
        }
        return httpStatus;
    }

    /**
     * GET /vocaBoostLists/:id -> get the "id" vocaBoostList.
     * 
     * @throws NotOwnerEception
     */
    @RequestMapping(value = "/vocaBoostLists/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VocaBoostList> get(@PathVariable String id) throws NotOwnerException {
        LOGGER.debug("REST request to get VocaBoostList : {}", id);
        return Optional.ofNullable(vocaBoostListService.findOne(id)).map(vocaBoostList -> new ResponseEntity<>(vocaBoostList, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * UPDATE /vocaBoostLists -> update a vocaBoostList.
     */
    @RequestMapping(value = "/vocaBoostLists", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void update(@RequestBody VocaBoostList vocaBoostList) {
        LOGGER.debug("REST request to update vocaBoostList : {}", vocaBoostList.getName());

        vocaBoostListService.save(vocaBoostList);
    }

    /**
     * DELETE /vocaBoostLists/:id -> delete the "id" vocaBoostList.
     * 
     * @throws NotOwnerEception
     */
    @RequestMapping(value = "/vocaBoostLists/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable String id) throws NotOwnerException {
        LOGGER.debug("REST request to delete VocaBoostList : {}", id);
        vocaBoostListService.delete(id);
    }

    /**
     * GET /vocaBoostLists/csv/:id -> get the csv export of "id" vocaBoostList.
     * 
     * @throws NotOwnerEception
     */
    @RequestMapping(value = "/vocaBoostLists/csv/{id}", method = RequestMethod.GET, produces = "application/octetstream")
    @Timed
    public String exportCSV(HttpServletResponse response, @PathVariable String id) throws NotOwnerException {
        LOGGER.info("REST request to export CSV file of vocaBoostList {}", id);

        String string = null;

        VocaBoostList vocaBoostList = vocaBoostListService.findOne(id);
        if (vocaBoostList != null) {
            response.setHeader("Content-disposition", "attachment;filename="
                    + vocaBoostList.getName() + "_" + vocaBoostList.getLanguage().getDisplayName() + ".csv");

            try {
                string = vocaBoostListService.exportCSV(id);
            } catch (IOException ioe) {
                log.error(ioe.getMessage());
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }

        return string;
    }

    /**
     * Generate the headers for a get all vocaBoostLists request
     * 
     * @param vocaBoostPage
     * @return
     */
    private HttpHeaders generatePaginatedGetHeaders(Page<VocaBoostList> vocaBoostPage) {
        int offset = 0;
        int limit = 0;
        if (vocaBoostPage.getNumberOfElements() > 0) {
            // Index of the first element returned
            offset = vocaBoostPage.getNumber() * vocaBoostPage.getSize();
            // Index of the last element returned
            limit = offset + vocaBoostPage.getNumberOfElements() - 1;
        }

        // HttpHeader : Content-Range and Accept-Range for the paginated GET
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Range", offset + "-" + limit + "/" + vocaBoostPage.getTotalElements());
        headers.add("Accept-Range", "vocaBoostList 50");
        return headers;
    }

    @ExceptionHandler(NotOwnerException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    private @ResponseBody String handleNotOwnerException(NotOwnerException noe) {
        LOGGER.warn(noe.getMessage());
        return noe.getMessage();
    }

}
