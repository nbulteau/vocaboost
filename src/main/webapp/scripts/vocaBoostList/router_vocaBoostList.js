'use strict';

vocaboostApp.config(function($routeProvider, $httpProvider, $translateProvider,
		USER_ROLES) {
	$routeProvider.when('/vocaBoostList', {
		templateUrl : 'views/vocaboostLists.html',
		controller : 'VocaBoostListController',
		access : {
			authorizedRoles : [ USER_ROLES.all ]
		}
	}).when('/vocaBoostList/add', {
		templateUrl : 'views/vocaboostListEdit.html',
		controller : 'VocaBoostListEditController',
		access : {
			authorizedRoles : [ USER_ROLES.all ]
		}
	}).when('/vocaBoostList/edit/:id', {
		templateUrl : 'views/vocaboostListEdit.html',
		controller : 'VocaBoostListEditController',
		access : {
			authorizedRoles : [ USER_ROLES.all ]
		}
	}).when('/vocaBoostList/test/:id', {
		templateUrl : 'views/vocaboostListTest.html',
		controller : 'VocaBoostListTestController',
		access : {
			authorizedRoles : [ USER_ROLES.all ]
		}
	})
});
