'use strict';

vocaboostApp.factory('VocaBoostList', function ($resource) {
        return $resource('api/v1/vocaBoostLists/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'update': { method: 'PUT'},
            'search' : {
                method : 'GET',
                isArray : true,
                params : {
                    page : '@page',
                    pageSize : '@size'
                }
            }
        });
    });
