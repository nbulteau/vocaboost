'use strict';

vocaboostApp.controller('VocaBoostListController', function($scope, $rootScope, $location, dialogs, VocaBoostList) {

    VocaBoostList.query(function(result, responseHeader) {
        var contentRange = responseHeader('Content-Range').split(/-|\//);
        $scope.vocaBoostLists = result;
    });

    $scope.create = function() {
        VocaBoostList.save($scope.vocaBoostList, function() {
            $scope.page = VocaBoostList.query(function(result, responseHeader) {
                var contentRange = responseHeader('Content-Range').split(/-|\//);
                $scope.vocaBoostLists = result;
            });

            $('#saveVocaBoostListModal').modal('hide');
            $scope.clear();
        });
    };

    $scope.update = function(id) {
        $scope.vocaBoostList = VocaBoostList.get({
            id : id
        });

        $('#saveVocaBoostListModal').modal('show');
    };

    $scope.remove = function(id) {
        var opts = {
            'size' : 'sm'
        }

        var dlg = dialogs.confirm('Please Confirm', 'Are you sure?', opts);
        dlg.result.then(function(btn) {
            VocaBoostList.remove({
                'id' : id
            }, function() {
                $scope.page = VocaBoostList.query(function(result, responseHeader) {
                    var contentRange = responseHeader('Content-Range').split(/-|\//);
                    $scope.vocaBoostLists = result;
                });
            });
        });
    };

    // Navigation
    $scope.test = function(id) {
        $location.path("/vocaBoostList/test/" + id);
    };
    $scope.edit = function(id) {
        $location.path("/vocaBoostList/edit/" + id);
    };
    $scope.add = function() {
        $location.path("/vocaBoostList/add");
    };
});

vocaboostApp.controller('VocaBoostListEditController',
        function($scope, $location, $routeParams, dialogs, VocaBoostList) {

            $scope.counter = 0;

            $scope.updateMode = false;

            $scope.itemsInTheCurrentPage = [];

            // pagination
            $scope.paginate = function() {
                var begin = ($scope.currentPage - 1) * $scope.itemsPerPage;
                var end = begin + $scope.itemsPerPage;
                $scope.itemsInTheCurrentPage = $scope.vocaBoostList.vocaBoostItems.slice(begin, end);
            }

            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;

            // pager
            $scope.maxSize = 5;
            $scope.bigTotalItems = 50;
            $scope.bigCurrentPage = 1;

            $scope.setPage = function(pageNo) {
                $scope.currentPage = pageNo;
                $scope.paginate();
            };

            $scope.pageChanged = function() {
                console.log('Page changed to: ' + $scope.currentPage);
                $scope.paginate();
            };

            if ($routeParams.id) {
                $scope.updateMode = true;
            }

            if ($scope.updateMode) {
                $scope.vocaBoostList = VocaBoostList.get({
                    'id' : $routeParams.id
                }, function() {
                    $scope.setPage(1);
                });
            } else {
                $scope.vocaBoostList = {
                    'name' : '',
                    'description' : '',
                    'language' : 'en',
                    'sharingMode' : 'PRIVATE',
                    'vocaBoostItems' : []
                };
            }

            $scope.sharingModes = [ 'PRIVATE', 'PUBLIC' ];

            $scope.languages = [ 'en', 'es', 'it' ];

            // vocaBoostItem
            $scope.updateVocaBoostItemMode = false;

            $scope.addVocaBoostItem = function() {
                $scope.vocaBoostItem = {
                    'id' : 'newId' + $scope.counter++,
                    'toLearn' : '',
                    'answer' : ''
                };

                $scope.updateVocaBoostItemMode = false;
                $('#vocaBoostItemModal').modal('show');
            };

            $scope.updateVocaBoostItem = function(vocaBoostItem) {
                $scope.vocaBoostItem = angular.copy(vocaBoostItem);
                $scope.updateVocaBoostItemMode = true;
                $('#vocaBoostItemModal').modal('show');
            };

            $scope.removeVocaBoostItem = function(vocaBoostItemToDel) {
                var opts = {
                    'size' : 'sm'
                }
                var dlg = dialogs.confirm('Please Confirm', 'Are you sure?', opts);
                dlg.result.then(function(btn) {
                    // remove it from the list
                    $scope.vocaBoostList.vocaBoostItems = $scope.vocaBoostList.vocaBoostItems.filter(function(
                            vocaBoostItem) {
                        return vocaBoostItem.id !== vocaBoostItemToDel.id;
                    });
                    $scope.paginate();
                });
            };

            $scope.cancelEditVocaBoostItem = function() {
                $scope.vocaBoostItem = null;
                $('#vocaBoostItemModal').modal('hide');
            }

            $scope.submitVocaBoostItem = function() {
                // Update VocaBoostItem
                if ($scope.updateVocaBoostItemMode) {
                    // we start to remove it from the list
                    $scope.vocaBoostList.vocaBoostItems = $scope.vocaBoostList.vocaBoostItems.filter(function(
                            vocaBoostItem) {
                        return vocaBoostItem.id !== $scope.vocaBoostItem.id;
                    });
                }

                // Add the vocaBoostItem
                $scope.vocaBoostList.vocaBoostItems.push($scope.vocaBoostItem);

                $scope.vocaBoostItem = null;
                $scope.paginate();
                $('#vocaBoostItemModal').modal('hide');
            };

            $scope.submitVocaBoostList = function() {
                if ($scope.updateMode) {
                    // Edit mode
                    VocaBoostList.update($scope.vocaBoostList, function() {
                        $location.path("/vocaBoostList");
                    });
                } else {
                    // Add mode
                    VocaBoostList.save($scope.vocaBoostList, function() {
                        $location.path("/vocaBoostList");
                    });
                }
            };

            // Navigation
            $scope.cancel = function() {
                $location.path("/vocaBoostList");
            };
            $scope.exportCSV = function(id) {
                $location.path("api/v1/vocaBoostList/csv/" + id);
            };

        });

vocaboostApp.controller('VocaBoostListTestController',
        function($scope, $location, $routeParams, dialogs, VocaBoostList) {

            $scope.sessionScore = undefined;

            $scope.started = false;

            $scope.flip = function() {
                if ($scope.flipped === '') {
                    $scope.flipped = 'flipped';
                } else {
                    $scope.flipped = '';
                }
            };

            if ($routeParams.id) {
                $scope.vocaBoostList = VocaBoostList.get({
                    'id' : $routeParams.id
                });
            }

            // Select and order the cards that will be part of the session
            // We don't use the spaced repetition algorithm
            $scope.start = function() {
                $scope.flipped = '';
                $scope.started = true;
                $scope.cardsToLearn = [];
                $scope.selectedCardsForSession = [];
                angular.forEach($scope.vocaBoostList.vocaBoostItems, function(vocaBoostItem) {
                    var card = {
                        'vocaBoostItem' : vocaBoostItem,
                        'lastAnswersInSession' : [],
                        'repeatCounter' : 0,
                        'repeatCounterTotal' : 0
                    }
                    $scope.cardsToLearn.push(card);

                    $scope.selectedCardsForSession.push(card);

                });
                $scope.max = $scope.selectedCardsForSession.length;
                $scope.moveToNextCard();
                $scope.computeStatsForSession();
            };

            // move to next card
            $scope.moveToNextCard = function() {
                $scope.currentStats = $scope.computeStatsForSession();
                if ($scope.cardsToLearn.length > 0) {
                    $scope.curIndex = Math.floor((Math.random() * $scope.cardsToLearn.length));
                    $scope.currentCard = $scope.cardsToLearn[$scope.curIndex];
                    if ('speechSynthesis' in window) {
                        speetchToText();
                    }
                } else {
                    $scope.started = false;
                    $scope.computeSessionScore();
                }
            };

            $scope.correct = function() {
                $scope.markCurrentCard(true);
                $scope.moveToNextCard();
            };

            $scope.wrong = function() {
                $scope.markCurrentCard(false);
                $scope.moveToNextCard();
            };

            $scope.markCurrentCard = function(iKnow) {
                // Should we remove the cards from the session
                var removeCardFromSession = false;

                // Save the last answers to calculate the score
                // note : this one is transient and not saved
                $scope.currentCard.lastAnswersInSession.push(iKnow);

                var repeatCounter = $scope.currentCard.repeatCounter;
                var repeatCounterTotal = $scope.currentCard.repeatCounterTotal;

                // Compute new score
                if (repeatCounter === undefined) {
                    repeatCounter = 0;
                }

                repeatCounter++;

                if (iKnow === true) {
                    $scope.currentCard.repeatCounter = repeatCounter;
                    // Removing card from session if necessary
                    $scope.cardsToLearn.splice(this.curIndex, 1);
                } else {
                    $scope.currentCard.repeatCounter = 1;
                }
            };

            $scope.computeStatsForSession = function() {
                // Json to be returned with count of cards in each category
                var stats = {
                    progress : 0,
                    known : 0,
                    success : 0,
                    fail : 0,
                    notYetViewed : 0
                }

                // Iterates on the all the cards selected for session
                // Note : some cards may have not yet been viewed
                angular.forEach($scope.selectedCardsForSession, function(card) {
                    var isNotYetViewed = (card.lastAnswersInSession.length === 0);
                    var isFail, isSuccess, isKnown;

                    if (isNotYetViewed) {
                        isFail = isSuccess = isKnown = false;
                    } else {
                        isFail = isSuccess = isKnown = true;
                        for (var i = 0; i < card.lastAnswersInSession.length; i++) {
                            var curResult = card.lastAnswersInSession[i];

                            // At least a false, can't be known
                            if (curResult == false) {
                                isKnown = false;
                            } else {
                                // At least a true, can't be failed and probably a success
                                isFail = false;
                            }
                        }

                        // If not a fail and not a known, then it's a success otherwise isSuccess is false
                        isSuccess = (!isKnown && !isFail);
                    }

                    // Allocate the points
                    if (isKnown) {
                        stats.known++;
                    } else if (isSuccess) {
                        stats.success++;
                    } else if (isFail) {
                        stats.fail++;
                    } else if (isNotYetViewed) {
                        stats.notYetViewed++
                    }
                    stats.progressKnown = Math.round(stats.known * 100 / $scope.max);
                    stats.progressSuccess = Math.round(stats.success * 100 / $scope.max);
                });

                console.log(stats);

                return stats;
            };

            $scope.computeSessionScore = function() {
                var stats = $scope.computeStatsForSession();
                $scope.sessionScore = Math.floor(stats.known
                        / (stats.known + stats.success + stats.fail + stats.notYetViewed) * 100);
            };

            var speetchToText = function() {
                var voices = window.speechSynthesis.getVoices();
                console.log(voices);
                var msg = new SpeechSynthesisUtterance($scope.currentCard.vocaBoostItem.toLearn);

                if ($scope.vocaBoostList.language === 'en') {
                    msg.voice = voices[3]; // "Google UK English Female";
                } else if ($scope.vocaBoostList.language === 'es') {
                    msg.voice = voices[5]; // "Google español";
                } else if ($scope.vocaBoostList.language === 'it') {
                    msg.voice = voices[10]; // "Google italiano";
                }

                msg.lang = $scope.vocaBoostList.language;
                window.speechSynthesis.speak(msg);
            }

            // Navigation
            $scope.finish = function() {
                // Save the score
                $location.path("/vocaBoostList");
            }

});
