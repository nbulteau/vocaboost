package fr.nbu.vocaboost.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import fr.nbu.vocaboost.Application;
import fr.nbu.vocaboost.domain.SharingMode;
import fr.nbu.vocaboost.domain.VocaBoostList;
import fr.nbu.vocaboost.repository.VocaBoostListRepository;
import fr.nbu.vocaboost.service.VocaBoostListService;

/**
 * Test class for the VocaBoostListResource REST controller.
 *
 * @see VocaBoostListResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ToLearnListResourceTest {

    private static final String DEFAULT_NAME = "SAMPLE_NAME";
    private static final String UPDATED_NAME = "UPDATED_NAME";

    private static final String DEFAULT_DESCRIPTION = "SAMPLE_TEXT";
    private static final String UPDATED_DESCRIPTION = "UPDATED_TEXT";

    private static final String DEFAULT_USER = "User";

    private static final Locale DEFAULT_LANGUAGE = Locale.FRENCH;
    private static final Locale UPDATED_LANGUAGE = Locale.ENGLISH;

    @Inject
    private VocaBoostListService vocaBoostListService;

    @Inject
    private VocaBoostListRepository vocaBoostListRepository;

    private MockMvc restToLearnListMockMvc;

    private VocaBoostList vocaBoostList;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VocaBoostListResource vocaBoostListResource = new VocaBoostListResource();
        ReflectionTestUtils.setField(vocaBoostListResource, "vocaBoostListService", vocaBoostListService);
        this.restToLearnListMockMvc = MockMvcBuilders.standaloneSetup(vocaBoostListResource).build();
    }

    @Before
    public void initTest() {
        vocaBoostListRepository.deleteAll();
        vocaBoostList = new VocaBoostList();
        vocaBoostList.setName(DEFAULT_NAME);
        vocaBoostList.setDescription(DEFAULT_DESCRIPTION);
        vocaBoostList.setLanguage(DEFAULT_LANGUAGE);
        vocaBoostList.setSharingMode(SharingMode.PRIVATE);
        vocaBoostList.setCreatedBy(DEFAULT_USER);

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = new TestingAuthenticationToken(DEFAULT_USER, "");
        securityContext.setAuthentication(authentication);
    }

    @Test
    public void createToLearnList() throws Exception {
        // Validate the database is empty
        assertThat(vocaBoostListRepository.findAll()).hasSize(0);

        // Create the VocaBoostList
        restToLearnListMockMvc.perform(
            post("/api/v1/vocaBoostLists").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(vocaBoostList)))
            .andExpect(status().isCreated());

        // Validate the VocaBoostList in the database
        List<VocaBoostList> vocaBoostLists = vocaBoostListRepository.findAll();
        assertThat(vocaBoostLists).hasSize(1);
        VocaBoostList testToLearnList = vocaBoostLists.iterator().next();
        assertThat(testToLearnList.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testToLearnList.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testToLearnList.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
    }

    @Test
    public void getAllToLearnLists() throws Exception {
        // Initialize the database
        vocaBoostListRepository.save(vocaBoostList);

        // Get all the toLearnLists
        restToLearnListMockMvc.perform(get("/api/v1/vocaBoostLists")).andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.[0].id").value(vocaBoostList.getId()))
            .andExpect(jsonPath("$.[0].name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.[0].description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.[0].language").value(DEFAULT_LANGUAGE.toString()));
    }

    @Test
    public void getToLearnList() throws Exception {
        // Initialize the database
        vocaBoostListRepository.save(vocaBoostList);

        // Get the vocaBoostList
        restToLearnListMockMvc.perform(get("/api/v1/vocaBoostLists/{id}", vocaBoostList.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id").value(vocaBoostList.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()));
    }

    @Test
    public void getNonExistingToLearnList() throws Exception {
        // Get the vocaBoostList
        restToLearnListMockMvc.perform(get("/api/v1/vocaBoostLists/{id}", 1L)).andExpect(status().isNotFound());
    }

    @Test
    public void updateToLearnList() throws Exception {
        // Initialize the database
        vocaBoostListRepository.save(vocaBoostList);

        // Update the vocaBoostList
        vocaBoostList.setName(UPDATED_NAME);
        vocaBoostList.setDescription(UPDATED_DESCRIPTION);
        vocaBoostList.setLanguage(UPDATED_LANGUAGE);
        restToLearnListMockMvc.perform(
            put("/api/v1/vocaBoostLists").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(vocaBoostList))).andExpect(
            status().isOk());

        // Validate the VocaBoostList in the database
        List<VocaBoostList> vocaBoostLists = vocaBoostListRepository.findAll();
        assertThat(vocaBoostLists).hasSize(1);
        VocaBoostList testToLearnList = vocaBoostLists.iterator().next();
        assertThat(testToLearnList.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testToLearnList.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testToLearnList.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        ;
    }

    @Test
    public void deleteToLearnList() throws Exception {
        // Initialize the database
        vocaBoostListRepository.save(vocaBoostList);

        // Get the vocaBoostList
        restToLearnListMockMvc.perform(delete("/api/v1/vocaBoostLists/{id}", vocaBoostList.getId()).accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(
            status().isOk());

        // Validate the database is empty
        List<VocaBoostList> vocaBoostLists = vocaBoostListRepository.findAll();
        assertThat(vocaBoostLists).hasSize(0);
    }
}
