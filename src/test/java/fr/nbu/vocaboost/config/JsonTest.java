package fr.nbu.vocaboost.config;

import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.nbu.vocaboost.domain.SharingMode;
import fr.nbu.vocaboost.domain.VocaBoostItem;
import fr.nbu.vocaboost.domain.VocaBoostList;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
// @WebAppConfiguration
//@ActiveProfiles("test")
public class JsonTest {

    // @Autowired
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testJsonToStringVocaBoostList() throws IOException {
        Set<VocaBoostItem> vocaBoostItems = new HashSet<>();
        vocaBoostItems.add(new VocaBoostItem("toLearn", "answer"));

        VocaBoostList vocaBoostList = new VocaBoostList();
        vocaBoostList.setLanguage(Locale.FRENCH);
        vocaBoostList.setName("name");
        vocaBoostList.setSharingMode(SharingMode.PRIVATE);
        vocaBoostList.setDescription("description");
        vocaBoostList.setVocaBoostItems(vocaBoostItems);
        String json = objectMapper.writeValueAsString(vocaBoostList);

        System.out.println(json);
    }

}
